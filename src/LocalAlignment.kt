import java.io.File

class LocalAlignment{
    private val gapSymbol: Char = '_'
    private val lookup: HashMap<Char, Int> = hashMapOf('a' to 0, 'c' to 1, 't' to 2, 'g' to 3)
    var n: Int = 5

    private var str1: String = ""
    private var str2: String = ""
    private var similarityMatrix = Array(
        lookup.size + 1,
        {IntArray(lookup.size + 1)}
    )
    private var memoryTable = Array(0, {i -> IntArray(0)})

    private val cu = CommonUtils(lookup, similarityMatrix)

    fun readFileAndFillInData(fileName: String) {
        val lineList = mutableListOf<String>()

        File(fileName).useLines { lines -> lines.forEach { lineList.add(it) } }

        str1 = lineList[0]
        str2 = lineList[1]

        for (i in 2 until lineList.size) {
            similarityMatrix[i - 2] = lineList[i].split(",").map { it.toInt() }.toIntArray()
        }

        n = lineList.size - 2

        memoryTable = Array(str1.length + 1, {i -> IntArray(str2.length + 1)})
        str1 = str1.toLowerCase().trim().replace(" ", "")
        str2 = str2.toLowerCase().trim().replace(" ", "")

        for (i in 1 until str1.length + 1){
            for( j in 1 until str2.length + 1){
                val str1Letter = str1[i - 1]
                val str2Letter = str2[j - 1]
                val bothAligned = cu.getSimilarity(str1Letter, str2Letter) + memoryTable[i-1][j-1]
                val str1WithGap = cu.getGapPrize(true, str2Letter) + memoryTable[i-1][j]
                val str2WithGap = cu.getGapPrize(false, str1Letter) + memoryTable[i][j-1]
                val prizesList = listOf(bothAligned, str1WithGap, str2WithGap, 0)
                val maxValue = prizesList.max() ?: 0
                memoryTable[i][j] = maxValue
            }
        }
    }

    fun findAlignment() {
        var str1Out = ""
        var str2Out = ""

        val maxIndexesAndValue = cu.getMaxOf2dArray(memoryTable)
        var i = maxIndexesAndValue[0]
        var j = maxIndexesAndValue[1]

        println("Max value for local alignment: " + maxIndexesAndValue[2])

        while(i > 0 && j > 0){
            val currentValue = memoryTable[i][j]
            val topLeftValue = memoryTable[i-1][j-1]
            val leftValue = memoryTable[i][j-1]
            val topValue = memoryTable[i-1][j]

            val str1Letter = str1[i - 1]
            val str2Letter = str2[j - 1]

            if(currentValue - cu.getSimilarity(str1Letter, str2Letter) == topLeftValue){
                str1Out = str1Letter + str1Out
                str2Out = str2Letter + str2Out
                i -= 1
                j -= 1
            }
            else if(currentValue - cu.getGapPrize(true, str2Letter) == topValue){
                str1Out = str1Letter + str1Out
                str2Out = gapSymbol + str2Out
                i -= 1
            }
            else if(currentValue - cu.getGapPrize(false, str1Letter) == leftValue){
                str1Out = gapSymbol + str1Out
                str2Out = str2Letter + str2Out
                j -= 1
            }
            else {
                break
            }
        }
        println("Optimal local alignment: \n" + str1Out + "\n" + str2Out + "\n")
    }
}