import java.io.File

class GlobalSimilarityAlignment{
    private val gapSymbol: Char = '_'
    private val lookup: HashMap<Char, Int> = hashMapOf('a' to 0, 'c' to 1, 't' to 2, 'g' to 3)
    var n: Int = 5 // change to 21? based on file?

    private var str1: String = ""
    private var str2: String = ""
    private var similarityMatrix = Array(
        lookup.size + 1,
        {IntArray(lookup.size + 1)}
    )
    private var memoryTable = Array(0, {i -> IntArray(0)})

    fun readFileAndFillInData(fileName: String) {
        val lineList = mutableListOf<String>()

        File(fileName).useLines { lines -> lines.forEach { lineList.add(it) } }

        str1 = lineList[0]
        str2 = lineList[1]

        for (i in 2 until lineList.size) {
            similarityMatrix[i - 2] = lineList[i].split(",").map { it.toInt() }.toIntArray()
        }

        n = lineList.size - 2

        memoryTable = Array(str1.length, {i -> IntArray(str2.length)})
    }

    fun getSimilarity(c1: Char, c2: Char): Int{
        var i = lookup[c1]
        var j = lookup[c2]
        i = i ?: -1
        j = j ?: -1
        if (i == -1 || j == -1){
            throw Exception("invalid letters in your strings: " + str1 + " or " + str2 + ". You can choose from: " + lookup.keys.toString())
        }
        return similarityMatrix[i][j]
    }

    fun getGapPrize(firstStrWithGap: Boolean, c: Char): Int{
        var index = lookup[c]
        index = index ?: -1

        return if (firstStrWithGap)
            similarityMatrix.last()[index]
        else
            similarityMatrix[index].last()

    }

    fun prepareMemoryTable() {
        str1 = str1.toLowerCase().trim().replace(" ", "")
        str2 = str2.toLowerCase().trim().replace(" ", "")

        for(i in 0 until str1.length - 1){
            memoryTable[i][0] = getSimilarity(str1[i], str2[0])
        }
        memoryTable[str1.length - 1][0] = getGapPrize(true, str2[0])

        for(j in 0 until str2.length - 1){
            memoryTable[0][j] = getSimilarity(str1[0], str2[j])
        }
        memoryTable[0][str2.length - 1] = getGapPrize(false, str1[0])


        for (i in 1 until str1.length){
            for( j in 1 until str2.length){
                var bothAligned = getSimilarity(str1[i], str2[j]) + memoryTable[i-1][j-1]
                var str1WithGap = getGapPrize(true, str2[j]) + memoryTable[i-1][j]
                var str2WithGap = getGapPrize(false, str1[i]) + memoryTable[i][j-1]
                memoryTable[i][j] = maxOf(bothAligned, str1WithGap, str2WithGap)
            }
        }
    }

    fun findAlignment() {
        var str1Out = ""
        var str2Out = ""

        var i = str1.length - 1
        var j = str2.length - 1

        while(i > 0 && j > 0){
            if(memoryTable[i][j] - getSimilarity(str1[i], str2[j]) == memoryTable[i-1][j-1]){
                str1Out = str1[i] + str1Out
                str2Out = str2[j] + str2Out
                i -= 1
                j -= 1
            }
            else if(memoryTable[i][j] - getGapPrize(true, str2[j]) == memoryTable[i-1][j]){
                str1Out = str1[i] + str1Out
                str2Out = gapSymbol + str2Out
                i -= 1
            }
            else if(memoryTable[i][j] - getGapPrize(false, str1[i]) == memoryTable[i][j-1]){
                str1Out = gapSymbol + str1Out
                str2Out = str2[j] + str2Out
                j -= 1
            }
        }

        while(i >= 0 || j >= 0){
            str1Out = if (i < 0) gapSymbol + str1Out else str1[i] + str1Out
            str2Out = if (j < 0) gapSymbol + str2Out else str2[j] + str2Out
            i -= 1
            j -= 1
        }

        println("\nOptimal Alignment: \n" + str1Out + "\n" + str2Out + "\n")
    }
}