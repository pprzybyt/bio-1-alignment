class CommonUtils(private val lookup: HashMap<Char, Int>, private val similarityMatrix: Array<IntArray>) {
    fun getMaxOf2dArray (array2D: Array<IntArray>): List<Int> {
        var i = 0
        var j = 0
        var max = Int.MIN_VALUE

        array2D.forEachIndexed { index, arrayRow: IntArray ->
            val localMax = arrayRow.max() ?:0
            if (localMax > max) {
                max = localMax
                i = index
                j = arrayRow.indexOf(max)
            }
        }
        return listOf(i, j, max)
    }

    fun getGapPrize(firstStrWithGap: Boolean, c: Char): Int{
        var index = lookup[c]
        index = index ?: -1

        return if (firstStrWithGap)
            similarityMatrix.last()[index]
        else
            similarityMatrix[index].last()

    }

    fun getSimilarity(c1: Char, c2: Char): Int{
        var i = lookup[c1]
        var j = lookup[c2]
        i = i ?: -1
        j = j ?: -1
        if (i == -1 || j == -1){
            throw Exception("invalid letters in your strings. You can choose from: " + lookup.keys.toString())
        }
        return similarityMatrix[i][j]
    }
}



