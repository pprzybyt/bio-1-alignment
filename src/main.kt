// str1 -> vertical
// str2 -> horizontal


fun main(args : Array<String>) {
    val gsa = GlobalSimilarityAlignment()
    gsa.readFileAndFillInData(System.getProperty("user.dir") + "\\src\\data.txt")
    gsa.prepareMemoryTable()
    gsa.findAlignment()

    val la = LocalAlignment()
    la.readFileAndFillInData(System.getProperty("user.dir") + "\\src\\data.txt")
    la.findAlignment()

}